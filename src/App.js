import React, { Component } from 'react';
import './App.css';
import Navigation from './components/Navigation';
import {courses} from './todos.json';
console.log(courses);

class App extends Component {
    constructor()
    {
      super();
        this.state = {
            courses
        }
    }
  render() {

        const courses = this.state.courses.map((course, i) => {
            return (
                <div className="col-md-4" key={i}>
                <div className="card mt-4">
                    <div className="card-header">
                        <h3>{course.name}</h3>
                    </div>
                    <div className="card-body">
                        <p>{course.description}</p>
                    </div>
                </div>
                </div>
            )
        })
    return (
      <div className="App">
        <Navigation titulo="Welcome to Tergar Community"/>
          <h1> Courses list</h1>
          <div className="container">
              <div className="row">
                  { courses }
              </div>
          </div>
      </div>
    );
  }
}

export default App;
