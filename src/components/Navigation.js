import React, { Component } from 'react';
import logo from "../header-logo2.png";

class Navigation extends Component{

    render()
    {
        return (
        <nav className="navbar bg-red">
            <img src={logo} className="App-logo" alt="logo" />
            <h4>{ this.props.titulo}</h4>
        </nav>
        )
    }
}

export default Navigation;